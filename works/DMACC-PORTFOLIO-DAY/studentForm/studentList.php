<?php
//session_start();
//if ($_SESSION['validUser'] == "yes")	//If this is a valid user allow access to this page
//{	
	include 'connection.php';			//connects to the database
	
	$sql = "SELECT student_ID,student_firstName,student_lastName,student_emailAddress,student_hometown,student_programStudy,student_personalWebsite,student_careerGoals,student_threeWords,student_mainImage,student_popupImage FROM student_db";	// SQL command
	
	$query = $conn->prepare($sql) or die("<h1>Prepare Error</h1>");	//prepare the Statement Object
	
	//run the statement
	
	if( $query->execute() )	//Run Query and Make sure the Query ran correctly
	{
		$query->bind_result($student_ID,$student_firstName,$student_lastName,$student_emailAddress,$student_hometown,$student_programStudy,$student_personalWebsite,$student_careerGoals,$student_threeWords,$student_mainImage,$student_popupImage);
	
		$query->store_result();
	}
	else
	{
		//Problems were encountered.
		$message = "<h1 style='color:red'>Houston, We have a problem!</h1>";	
		$message .= "mysqli_error($conn)";		//Display error message information	
		echo $message;
	}
//}//end Valid User True
//else
//{
	//Invalid User attempting to access this page. Send person to Login Page
//	header('Location: presentersLogin.php');
//}
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Student Form</title>

<link rel="stylesheet" type="text/css" href="listCSS.css">
</head>
<body>
  <div>
  <img src="images/logo.png" height="200px" width="200px" class="center">

<div>
	<table border="1">
	<tr>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Email</th>
		<th>Hometown</th>
		<th>Program</th>
		<th>Website</th>
		<th>Goals</th>
		<th>Three Words</th>
		<th>Edit</th>
		<th>Delete</th>
	</tr>    
<?php
	//Display rows of data in table
	while( $query->fetch() )		
	//Turn each row of the result into an associative array 
  	{
		//For each row you have in the array create a table row
  		echo "<tr>";
  		echo "<td>$student_firstName</td>";
  		echo "<td>$student_lastName</td>";
  		echo "<td>$student_emailAddress</td>";
  		echo "<td>$student_hometown</td>";
  		echo "<td>$student_programStudy</td>";
  		echo "<td>$student_personalWebsite</td>";
  		echo "<td>$student_careerGoals</td>";
  		echo "<td>$student_threeWords</td>";
		echo "<td><a href='studentUpdateForm.php?recId=$student_ID'>Update</a></td>";
		echo "<td><a href='studentDelete.php?recId=$student_ID'>Delete</a></td>";
  		echo "</tr>\n";
  	}
	$query->close();
	$conn->close();	//Close the database connection	
?>
	</table>
</div>
</body>
</html>