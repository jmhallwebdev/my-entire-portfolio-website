<?php
include 'php/connection.php';

$sql = "SELECT student_firstName, student_lastName, student_mainImage, student_programStudy, student_ID FROM student_db";

$res = $connection->query($sql);


$populateWeb = "";
$populateGraphic = "";
$populatePhoto = "";
$populateVideo = "";

  if($res)
  {
    
    if ($res->num_rows > 0) 
    {

      while($student = $res->fetch_assoc())


        if($student['student_programStudy'] == 'Web Development')
          {
            $populateWeb .= "<div class='col-lg-2 col-xs-6 if-Grid-Item'>";
            //CREATING IMG TAG
            $populateWeb .= "<img src='" . $student['student_mainImage'] . "'";
            $populateWeb .= "data-toggle='modal' data-target='#studentModal' class='img-responsive center-block studentSelect hoverGS img-circle' alt='" . $student['student_firstName'] . " " . $student['student_lastName'];
            $populateWeb .= "' value='" . $student['student_ID'] . "'>";
            $populateWeb .= "<p class='text-center'>" . $student['student_firstName'] . "<br>" . $student['student_lastName'];
            //END OF IMG TAG
            $populateWeb .= "</p>";
            $populateWeb .= "</div>";          
          }

        else if($student['student_programStudy'] == 'Graphic Design')
          {
            $populateGraphic .= "<div class='col-lg-2 col-xs-6 if-Grid-Item'>";
            //CREATING IMG TAG
            $populateGraphic .= "<img src='" . $student['student_mainImage'] . "'";
            $populateGraphic .= "data-toggle='modal' data-target='#studentModal' class='img-responsive center-block studentSelect hoverGS img-circle' alt='" . $student['student_firstName'] . " " . $student['student_lastName'];
            $populateGraphic .= "' value='" . $student['student_ID'] . "'>";
            $populateGraphic .= "<p class='text-center'>" . $student['student_firstName'] . "<br>" . $student['student_lastName'];
            //END OF IMG TAG
            $populateGraphic .= "</p>";
            $populateGraphic .= "</div>";          
          }
		  
		  else if($student['student_programStudy'] == 'Video Production')
          {
            $populateVideo .= "<div class='col-lg-2 col-xs-6 if-Grid-Item'>";
            //CREATING IMG TAG
            $populateVideo .= "<img src='" . $student['student_mainImage'] . "'";
            $populateVideo .= "data-toggle='modal' data-target='#studentModal' class='img-responsive center-block studentSelect hoverGS img-circle' alt='" . $student['student_firstName'] . " " . $student['student_lastName'];
            $populateVideo .= "' value='" . $student['student_ID'] . "'>";
            $populateVideo .= "<p class='text-center'>" . $student['student_firstName'] . "<br>" . $student['student_lastName'];
            //END OF IMG TAG
            $populateVideo .= "</p>";
            $populateVideo .= "</div>";          
          }

        else if($student['student_programStudy'] == 'Photography')
          {
            $populatePhoto .= "<div class='col-lg-2 col-xs-6 if-Grid-Item'>";
            //CREATING IMG TAG
            $populatePhoto .= "<img src='" . $student['student_mainImage'] . "'";
            $populatePhoto .= "data-toggle='modal' data-target='#studentModal' class='img-responsive center-block studentSelect hoverGS img-circle' alt='" . $student['student_firstName'] . " " . $student['student_lastName'];
            $populatePhoto .= "' value='" . $student['student_ID'] . "'>";
            $populatePhoto .= "<p class='text-center'>" . $student['student_firstName'] . "<br>" . $student['student_lastName'];
            //END OF IMG TAG
            $populatePhoto .= "</p>";
            $populatePhoto .= "</div>";          
          }

    }//END WHILE
  }
  else
  {
    
    $displayErrorMsg .= "<h3>Sorry there has been a problem.</h3><br><h3>Please Try Again Later.</h3>";
    $displayMsg .= "<p>" . mysqli_error($connection) . "</p>";      
  }
  $connection->close();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DMACC PORTFOLIO DAY</title>

<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- Bootstrap Required -->
    <link rel="stylesheet" href="inc/bootstrap/css/bootstrap.css">
    <script src="inc/bootstrap/js/bootstrap.js"></script>

<!-- Custom -->
     <!-- Fonts -->
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
     <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
     <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
     
     <!-- FONT INFO BELOW -->
     <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">

     <!-- font-family: 'Questrial', sans-serif; -->

     <!-- CSS -->
     <link href="css/navBar2.css" rel="stylesheet">
     <link href="css/header2.css" rel="stylesheet">
     <link href="css/body3.css" rel="stylesheet">
     <link href="css/modal.css" rel="stylesheet">
     <!-- JS -->
     <script src="inc/jquery-1.11.2.min.js"></script>
     <script src="inc/jquery-stars.js"></script>
     <script src="inc/jquery.main.js"></script>
     <script type="text/javascript">

$(document).ready(function(){
	      jQuery('.example-box').jstars({
            image_path: 'imgs',
            image: 'jstar-modern.png',
            style: 'rand',
            frequency: 14
        });
})


//AJAX TO CHANGE BIO INFO
  $(document).ready(function() {

          $('.studentSelect').click(function() {
                var value = $(this).attr("value")
                $.ajax({
                      url: "php/bioQuery.php?value=" + value,
                      success: function(result) {

                      arrayRes = JSON.parse(result);
                      var arrayPrint = "";
                      var i = 0;
                      arrayPrint = arrayRes[i];
                      arrayPrint = arrayPrint.split("|");
          
                      var website = 'http://' + arrayPrint[5];
                      $("#name").html(arrayPrint[0] + " " + arrayPrint[1]);
                      $("#email").html(arrayPrint[2]);
                      $("#hometown").html(arrayPrint[3]);
                      $("#program").html(arrayPrint[4]);
                      $("#website").attr('href', website);
                      $("#careerGoals").html(arrayPrint[6]);
                      $("#threeWords").html(arrayPrint[7]);
                      $("#studentImg").attr('src', arrayPrint[8]);
                      $("#studentModalHeader").html(arrayPrint[4]);

                      }
                });
            });

  });

     </script>

</head>

<body>
<!--  Start of bio popup/modal-->
<div class="container">




<div class="modal fade" id="studentModal" tabindex="-1" role="dialog" aria-labelledby="studentModalHeader" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content background-color">
    <!--START OF MODAL HEADER -->
      <div class="modal-header header-color">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
          <h2 class="modal-title" id="studentModalHeader">Header</h2>
      </div>
      <!--BODY OF MODAL -->
      <div class="modal-body">
        <div class="row">
          <div class="col-md-5 img-modal-padding">
            <img id="studentImg" src="" height="250px">
          </div>
          <div class="col-md-6 middle-modal-padding">
            <h2 id="name">NAME</h2>
            <h4 id="program">PROGRAM</h4>
            <h4 id="email">EMAIL</h4>
            <h4><a id="website" href="" target="_blank">My Website</a></h4>
          </div>
        </div>
        
        <div class="lower-modal-padding">
        <div class="row">
          <h4>HOMETOWN</h4>
          <p id="hometown"></p>
        </div>
        <div class="row">
          <h4>CAREER GOALS</h4>
          <p id="careerGoals"></p>
        </div>
        <div class="row">
          <h4>Three Words to Describe Me</h4>
          <p id="threeWords"></p>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
<!-- END OF MODAL -->

<section id="reset-style" class="example-box blue">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand font-questrial" href="#page-top">DMACC Portfolio Day</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a class="font-questrial" href="#design">Graphic Design</a>
                    </li>
                    <li class="page-scroll">
                        <a class="font-questrial" href="#photography">Photography</a>
                    </li>
                    <li class="page-scroll">
                        <a class="font-questrial" href="#video">Video Production</a>
                    </li>
                    <li class="page-scroll">
                        <a class="font-questrial" href="#webDev">Web Development</a>
                    </li>
                    <li class="page-scroll">
                        <a class="font-questrial" href="#animation">Animation</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        <!-- /.container-fluid -->
    </nav>



<!--Header-->
    <header class="paddingHead">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3 col-md-10 col-md-offset-1 col-sm-12">
                        <img src="imgs/portfolioDayLogo.png" class="img-responsive center-block img-rotate" alt="Logo">
                </div>
            </div>
        </div>
    </header>
<!--End Header-->

<!--MainBody-->
  <div class="container">
    <div class="directions">
    <div class="row">
      <h1 class="if-majorHeader">DMACC PORTFOLIO DAY 2017</h1>
      <div class="col-lg-6 col-lg-offset-3">
      <h3 class="text-center">Thursday, April 13, 2017</h3>
      <h3 class="text-center">Noon - 5:00 P.M.</h3>
      <h3 class="text-center">Greater Des Moines Botanical Garden</h3>
      </div>
      <div class="col-lg-6 col-lg-offset-3">
      <p class="text-center">After two years of practicing our hex codes, sleight of hand, making layers disappear and magic wanding, we're ready to make the world a little more magical.</p>
      </div>
    </div><!--end directions div-->

    <div class="row">
      <div class="col-lg-6 col-lg-offset-3 ">
        <!-- <a href="#"> -->
         <!-- <div class="col-md-4 col-xs-6"> -->
           <!-- <p class="text-right">Download</p> -->
         <!-- </div> -->
         
         <!-- <div class="col-md-4 col-xs-6"> -->
            <p class="text-center"><a href="https://www.google.com/maps/place/Greater+Des+Moines+Botanical+Garden/@41.596713,-93.616382,17z/data=!3m1!4b1!4m5!3m4!1s0x87ee99a457093b8d:0x2e0ddf50113251e7!8m2!3d41.596709!4d-93.614188?hl=en" target="_blank">Click For Directions</a></p>
         <!-- </div> -->

         <div class="col-md-offset-4 col-md-4 visible-lg visible-md">
             <img src="imgs/fingerDown-01.png" class="img-responsive center-block" alt="Download Hand">
         </div>
      </div>
    </div>

</div>
<!--Main Bits-->

<!-- GRID 1 -->

<!--End GRID 1 -->

<!-- GRID 4-->
<div class="row">
      <div class="if-majorHeader">
         <div class="row">
         <div class="col-md-1 col-md-offset-3 visible-lg visible-md"><img src="imgs/swirlLeft-01.png" class="img-responsive center-block" alt="Side Doodle"></div>
         <div class="col-md-4 col-sm-12"><h2 class="text-center" id="design">Graphic Design</h2></div>
         <div class="col-md-1 visible-lg visible-md"><img src="imgs/swirRight-01.png" class="img-responsive center-block" alt="Side Doodle"></div>
         </div>
      </div>
</div>

<div class="row">
  <?php echo $populateGraphic; ?>
</div>
<!--End GRID 4 -->

<!-- GRID 5-->
<div class="row">
      <div class="if-majorHeader">
         <div class="row">
         <div class="col-md-1 col-md-offset-3 visible-lg visible-md"><img src="imgs/swirlLeft-01.png" class="img-responsive center-block" alt="Side Doodle"></div>
         <div class="col-md-4 col-sm-12"><h2 class="text-center" id="photography">Photography</h2></div>
         <div class="col-md-1 visible-lg visible-md"><img src="imgs/swirRight-01.png" class="img-responsive center-block" alt="Side Doodle"></div>
         </div>
      </div>
</div>

<div class="row">
  <?php echo $populatePhoto; ?>
</div>
<!--End GRID 5 -->

<!-- GRID 4-->
<div class="row">
      <div class="if-majorHeader">
         <div class="row">
         <div class="col-md-1 col-md-offset-3 visible-lg visible-md"><img src="imgs/swirlLeft-01.png" class="img-responsive center-block" alt="Side Doodle"></div>
         <div class="col-md-4 col-sm-12"><h2 class="text-center" id="video">Video Production</h2></div>
         <div class="col-md-1 visible-lg visible-md"><img src="imgs/swirRight-01.png" class="img-responsive center-block" alt="Side Doodle"></div>
         </div>
      </div>
</div>

<div class="row">
  <?php echo $populateVideo; ?>
</div>
<!--End GRID 4 -->

<!-- GRID 2 -->
<div class="row">
      <div class="if-majorHeader">
         <div class="row">
         <div class="col-md-1 col-md-offset-3 visible-lg visible-md"><img src="imgs/swirlLeft-01.png" class="img-responsive center-block" alt="Side Doodle"></div>
         <div class="col-md-4 col-sm-12"><h2 class="text-center" id="webDev">Web Development</h2></div>
         <div class="col-md-1 visible-lg visible-md"><img src="imgs/swirRight-01.png" class="img-responsive center-block" alt="Side Doodle"></div>
         </div>
      </div>
</div>

<div class="row">
  <?php echo $populateWeb; ?>
</div>

<!--
    IMAGES NEED data-toggle="modal" AND data-target="#studentModal" AND class="studentSelect" FOR BIO MODAL TO WORK PROPERLY. 
    IT ALSO NEEDS value="studentID value pulled my db" FOR THE AJAX CALL FOR THE BIO TO WORK. ASS OF RIGHT NOW IT ISN'T CONNECTED TO A DATABASE SO THE BIO AJAX WILL NOT PULL ANY DATA.


<!--End GRID 2 -->

<div class="row">
      <div class="if-majorHeader">
         <div class="row">
         <div class="col-md-1 col-md-offset-3 visible-lg visible-md"><img src="imgs/swirlLeft-01.png" class="img-responsive center-block" alt="Side Doodle"></div>
         <div class="col-md-4 col-sm-12"><h2 class="text-center" id="animation">Animation</h2></div>
         <div class="col-md-1 visible-lg visible-md"><img src="imgs/swirRight-01.png" class="img-responsive center-block" alt="Side Doodle"></div>
         </div>
      </div>
</div>

<!--
    IMAGES NEED data-toggle="modal" AND data-target="#studentModal" AND class="studentSelect" FOR BIO MODAL TO WORK PROPERLY. 
    IT ALSO NEEDS value="studentID value pulled my db" FOR THE AJAX CALL FOR THE BIO TO WORK. ASS OF RIGHT NOW IT ISN'T CONNECTED TO A DATABASE SO THE BIO AJAX WILL NOT PULL ANY DATA.

-->
<div class='row'>
  <p>Seven students graduated from the animation program with an Animation and Rich Media Diploma. The students include: Anna Blomgren, Sam Jones, Austin Ripperger, Alexis Robles, Tiffany Sinnott, Hailey Siska, Emily Thompson, and Josh Ziebell.</p>
</div>



<!--End Main Bits-->

  </div>
<!--End MainBody-->

<!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">

                    <ul class="list-inline text-center">
                    <li>
                    <p class="if-copyright">Des Moines Area Community College - Ankeny, IA</p>
                    </li>
                        <li>
                            <a href="https://www.facebook.com/dmaccportfolioday/">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                                    <i class="fa fa-facebook-square fa-stack-1x"></i>
                                </span>
                            </a>
                        </li>

                        <li>
                            <a href="https://www.instagram.com/dmaccportfolioday/">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                                    <i class="fa fa-instagram fa-stack-1x"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                    <p class="if-copyright copyright-center text-center">Copyright 2017 DMACC Portfolio Day</p>
                </div>
            </div>
        </div>
    </footer>
<!--End Footer-->
</section>
</body>
</html>