-- phpMyAdmin SQL Dump
-- version 4.3.7
-- http://www.phpmyadmin.net
--
-- Host: 10.123.0.133:3307
-- Generation Time: Apr 26, 2017 at 06:54 PM
-- Server version: 5.7.15
-- PHP Version: 5.4.45-0+deb7u4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gullydsm_dmacc`
--

-- --------------------------------------------------------

--
-- Table structure for table `student_db`
--

CREATE TABLE IF NOT EXISTS `student_db` (
  `student_ID` int(3) NOT NULL,
  `student_firstName` text NOT NULL,
  `student_lastName` text NOT NULL,
  `student_emailAddress` varchar(100) NOT NULL,
  `student_hometown` varchar(50) NOT NULL,
  `student_programStudy` varchar(50) NOT NULL,
  `student_personalWebsite` varchar(200) NOT NULL,
  `student_careerGoals` varchar(750) NOT NULL,
  `student_threeWords` text NOT NULL,
  `student_mainImage` varchar(100) NOT NULL,
  `student_popupImage` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_db`
--

INSERT INTO `student_db` (`student_ID`, `student_firstName`, `student_lastName`, `student_emailAddress`, `student_hometown`, `student_programStudy`, `student_personalWebsite`, `student_careerGoals`, `student_threeWords`, `student_mainImage`, `student_popupImage`) VALUES
(6, 'Jennifer', 'Warming', 'email@dmacc.edu', 'Des Moines, IA', 'Photography', 'jwarming.wix.com/portfolio', 'My career goals are to open my own portrait studio and photograph families, children, high school seniors and newborns. I want to work and surround myself with fun, exciting people and take beautiful images.\r\n\r\nSecond URL: <a href="http://jwarming.wixsite.com/photo" target="_blank">my site</a>', 'Happy, Hardworking, Friendly', 'imgs/studentImages/Jennifer Warming_165.jpg', 'imgs/studentImages/Jennifer Warming_250.jpg'),
(7, 'Taylor', 'Whipple', 'taylorgwhipple@gmail.com', 'Des Moines, IA', 'Graphic Design', 'tgwhipple.com', 'Design with purpose and inspire others.', 'Down-to-earth, Dedicated, Attentive', 'imgs/studentImages/Taylor Whipple_165.jpg', 'imgs/studentImages/Taylor Whipple_250.jpg'),
(8, 'Roxann', 'Vo', 'roxann.vo@hotmail.com', 'Vietnam', 'Graphic Design', 'www.roxannvo.us', 'I want to deal with marketing and advertising, like create banners, posters, vinyl, and illustrate images.', 'Outgoing, Friendly, Hardworker', 'imgs/studentImages/Roxann Vo_165.jpg', 'imgs/studentImages/Roxann Vo_165.jpg'),
(9, 'Vanessa', 'Vitelli', 'email@dmacc.edu', 'Riverside, CA', 'Photography', 'https://www.facebook.com/Unicorn.Occult.Photograph', 'Travel to distant places in hopes of my work being published by National Geographic. Other than that, I plan on opening a studio for commercial and portrait work as well as my side projects.', 'Creative, Enthusiastic, Brilliant', 'imgs/studentImages/default_image_165.jpg', 'imgs/studentImages/default_image_250.jpg'),
(10, 'Darrian', 'Rehan', 'email@dmacc.edu', 'Des Moines, IA', 'Photography', 'dmacc.edu', 'I don''t really have a career goal, but I do know I want to do something with commercial photography.', 'Patient, Kind, Happy', 'imgs/studentImages/Darrian Rehan_165.jpg', 'imgs/studentImages/Darrian Rehan_250.jpg'),
(11, 'Laura ', 'McCargar', 'lauranicolephotography@gmail.com', 'Ankeny, IA', 'Photography', 'dmacc.edu', 'I would like to own my own photography business, focusing on engagements and weddings.', 'Hard-working, Energetic, Adventurous', 'imgs/studentImages/Laura McCargar_165.jpg', 'imgs/studentImages/Laura McCargar_250.jpg'),
(12, 'Richard', 'Alvarenga-Lopez', 'email@dmacc.edu', 'Des Moines, IA', 'Photography', 'www.juxtra.net', 'Become a commercial and wedding photographer.', 'Funny, Outgoing, Friendly', 'imgs/studentImages/Richard Alarenga-Lopez_165.jpg', 'imgs/studentImages/Richard Alarenga-Lopez_250.jpg'),
(13, 'McKenzie', 'Wyatt', 'email@dmacc.edu', 'Mitcthellville, IA', 'Photography', 'www.McKenzieWyatt.com', 'I would like to be a freelance portrait and commercial photographer, and hopefully by doing the, I will acquire the opportunity to shoot for different brands and travel.', 'Creative, Detail-oriented, Genuine', 'imgs/studentImages/McKenzie Wyatt_165.jpg', 'imgs/studentImages/McKenzie Wyatt_250.jpg'),
(14, 'Trenton', 'Chesling', 'trentonchelsing@gmail.com', 'Indianola, IA', 'Photography', 'dmacc.edu', 'I would like to professionally work as a commercial and portrait photographer.', 'Direct, Genuine, Industrious', 'imgs/studentImages/Trent Chesling_165.jpg', 'imgs/studentImages/Trent Chesling_250.jpg'),
(15, 'Amy', 'Lamm', 'email@dmacc.edu', 'Ankeny, IA', 'Photography', 'dmacc.edu', 'My ultimate goal is to find a way to make a living capturing the beauty in this world through photography.', 'Creative, Free-spirited, Dedicated', 'imgs/studentImages/Amy Lamm_165.jpg', 'imgs/studentImages/Amy Lamm_250.jpg'),
(16, 'Maria', 'Cochran', 'email@dmacc.edu', 'Ames, IA', 'Photography', 'dmacc.edu', 'Have a small photo business specializing in weddings and seniors.', 'Curious, Reliable', 'imgs/studentImages/Maria Cochran_165.jpg', 'imgs/studentImages/Maria Cochran_250.jpg'),
(17, 'Steven', 'Hobort', 'email@dmacc.edu', 'Indianola, IA', 'Photography', 'SdHStudios.com', 'I would like to open a photography business in the Midwest and work on freelancing work as well.', 'Passionate, Ambitious, Outgoing', 'imgs/studentImages/Steven Hobart_165.jpg', 'imgs/studentImages/Steven Hobart_250.jpg'),
(19, 'Bethany ', 'Thompson', 'bethanythompson14@gmail.com', 'Clarion, IA', 'Graphic Design', 'bethanythompson.com', 'To work at a company with an awesome boss and co-workers.', 'Respectful, Nice, Caring', 'imgs/studentImages/Bethany Thompson_165.jpg', 'imgs/studentImages/Bethany Thompson_250.jpg'),
(20, 'Hannah ', 'Wright', 'email@dmacc.edu', 'Des Moines, IA', 'Video Production', 'hannahwright.org', 'I want to tell human-interest stories through video, photo, music, and word.', 'Learning, Awestruck, ENFP', 'imgs/studentImages/default_image_165.jpg', 'imgs/studentImages/default_image_250.jpg'),
(21, 'Jayden', 'Butler', 'jlbutler34@gmail.com', 'Slater, IA', 'Web Development', 'www.butlerjl.com', 'Work as a web developer in the Des Moines area, and continue to grow and expand my knowledge, while making a good living.', 'Teamwork, Calm, Resourceful ', 'imgs/studentImages/Jayden Butler_165.jpg', 'imgs/studentImages/Jayden Butler_250.jpg'),
(22, 'Travis', 'Crum', 'travisrcrum@gmail.com', 'Ames, IA', 'Web Development', 'buttonmashersmusic.com', 'I would like to work as a front end developer for a forward thinking company that will utilize my skill set and creativity. ', 'Creative, Motivated, Savvy', 'imgs/studentImages/Travis Crum_165.jpg', 'imgs/studentImages/Travis Crum_250.jpg'),
(23, 'Jeremy', 'Hall', 'info@jeremymhall.info', 'Des Moines, IA', 'Web Development', 'www.jeremymhall.info/portfolio/', 'Find employment as a web developer', 'Thorough, Detail-oriented', 'imgs/studentImages/Jeremy Hall_165.jpg', 'imgs/studentImages/Jeremy Hall_250.jpg'),
(24, 'Pammela ', 'Nelson', 'pammn1@msn.com', 'Hannibal, MO', 'Web Development', 'www.pjnelsongroup.net', 'Start a business in Web Development and communicate more effectively.', 'Creative, Positive, Energized', 'imgs/studentImages/Pammela Nelson_165.jpg', 'imgs/studentImages/Pammela Nelson_250.jpg'),
(25, 'Brad', 'Tingley', 'Bwtingley@gmail.com', 'Tama, IA', 'Web Development', 'www.Bwtingley.net', 'Get a job in web development or be a computer hardware technician.', 'Realistic, Engaged, Sharp', 'imgs/studentImages/Bradley Tingley_165.jpg', 'imgs/studentImages/Bradley Tingley_250.jpg'),
(26, 'Rachel', 'Rong', 'rachelrrong@gmail.com', 'Lincoln, NE', 'Graphic Design', 'RachelRenaRong.com', 'I want to remain in Iowa and would like to work for a publication design company.', 'Colorful, Dynamic Thinker, Bubbly ', 'imgs/studentImages/Rachel Rong_165.jpg', 'imgs/studentImages/Rachel Rong_250.jpg'),
(27, 'Brian ', 'Sandoval', 'briansandovalgd@gmail.com', 'Des Moines, IA', 'Graphic Design', 'dmacc.edu', 'My career goal is to one day start up my own design company. As an aspiring Graphic Designer and Photographer, I dream of seeing my art and designs around the world. This motivates me to continue creating daily.', 'n/a', 'imgs/studentImages/default_image_165.jpg', 'imgs/studentImages/default_image_250.jpg'),
(28, 'Lucas', 'Thul', 'lucasjthul@gmail.com', 'DeWitt, IA', 'Graphic Design', 'www.lucasthul.com', 'I want to do Graphic Design in the future.', 'Talented, Good Looking, Not Cocky', 'imgs/studentImages/Lucas Thul_165.jpg', 'imgs/studentImages/Lucas Thul_250.jpg'),
(29, 'Randy', 'Tharp', 'rstharp1@dmacc.edu', 'Des Moines, IA', 'Graphic Design', 'www.randyshanetharp.net', 'To design, layout, and illustrate for books.', 'Outspoken, Colorful, Approachable', 'imgs/studentImages/Randy Tharp_165.jpg', 'imgs/studentImages/Randy Tharp_250.jpg'),
(30, 'Julianna', 'Wegman', 'juliannawegman@gmail.com', 'Des Moines, IA', 'Graphic Design', 'juliewegman.info', 'I would like to work in a firm with something different to do every day.', 'Calm, Hard-working, Short', 'imgs/studentImages/Julianna Wegman_165.jpg', 'imgs/studentImages/Julianna Wegman_250.jpg'),
(31, 'Tyler ', 'Jurgensen', 'tyler.r.jurgensen@gmail.com', 'Ankeny, IA', 'Graphic Design', 'tylerrjurgensen.myportfolio.com', 'My main career goal is to work for a non-profit business/organization that helps local businesses with their company image. I also want to work with other designers in an effective team setting.', 'Creative, Caring, Funny', 'imgs/studentImages/Tyler Jurgensen_165.jpg', 'imgs/studentImages/Tyler Jurgensen_250.jpg'),
(32, 'Sara', 'Ladd', 'email@edu.com', 'Des Moines, IA', 'Graphic Design', 'www.saraskarsgard.com', 'While I am still undecided, I would like to focus on freelance or work for a company. Whatever path I choose, I know I want to excel in either career field. My love for Graphic Design comes from a mix of technology and art together.', 'N/A', 'imgs/studentImages/default_image_165.jpg', 'imgs/studentImages/default_image_250.jpg'),
(33, 'Marisa', 'Mueller', 'zoocrew05@msn.com', 'Urbandale, IA', 'Graphic Design', 'mnmueller.net', 'In the future I want to have a successful job in either print or web design. ', 'Hard-working, Caring, Goofy', 'imgs/studentImages/Marisa Mueller_165.jpg', 'imgs/studentImages/Marisa Mueller_250.jpg'),
(34, 'Kendalyn', 'Mulvey', 'kendaly.mulvey@outlook.com', 'Galesburg, IL', 'Graphic Design', 'kendalymulvey.com', 'I would love to create branding for music festivals, tours, bands, and films.', 'Sarcastic, Colorful, Caffeinated', 'imgs/studentImages/Kendalyn Mulvey_165.jpg', 'imgs/studentImages/Kendalyn Mulvey_250.jpg'),
(36, 'Austin ', 'Richards', 'owlscene@gmail.com', 'Iowa Falls, IA', 'Graphic Design', 'Austinrichardsdesign.com', 'I would like to eventually own my own business and do a little bit of everything within the Graphic Design business. ', 'Funny, Creative, Colourful', 'imgs/studentImages/Austin Richards_165.jpg', 'imgs/studentImages/Austin Richards_250.jpg'),
(37, 'Hannah', 'Rollins', 'hannaherollins@gmail.com', 'Ames, IA', 'Graphic Design', 'herdesign.myportfolio.com', 'My future career goals would be to land a job that uses creativity. I would want to work in a small business or freelance.', 'Funny, Creative, Outgoing', 'imgs/studentImages/Hannah Rollins_165.jpg', 'imgs/studentImages/Hannah Rollins_250.jpg'),
(40, 'Yuuki', 'Matsuyama', 'yuukimatsuyama.is@gmail.com', 'Ankeny, IA', 'Graphic Design', 'ymatsuyama.myportfolio.com', 'To design illustrations or layouts for a brand I believe in, and continue to better my craft. ', 'Exploratory, INTP, Pithy', 'imgs/studentImages/default_image_165.jpg', 'imgs/studentImages/default_image_250.jpg'),
(41, 'Ronnie', 'Miller', 'vermiller@gmail.com', 'West Des Moines, IA', 'Graphic Design', 'www.ronniemiller.net', 'If I can''t hack it as a professional fortune cookie writer, I will have to settle with a fulfilling career as a graphic designer/photographer/spy.', 'Sesquipedalianist, Antitransubstantiationalist, Hippopotomonstrosesquippedaliophobia', 'imgs/studentImages/Ronnie Miller_165.jpg', 'imgs/studentImages/Ronnie Miller_250.jpg'),
(42, 'Brin', 'Toland', 'brintoland@hotmail.com', 'Mt. Pleasant, IA', 'Graphic Design', 'bechance.com/brintoland', 'I would like to become a character designer for Bioware games in the future, and work overseas while gaining experience in several areas of design and character design.', 'Hard-working, Artistic, Creative', 'imgs/studentImages/Brin Toland_165.jpg', 'imgs/studentImages/Brin Toland_250.jpg'),
(43, 'Nicole', 'Axtell', 'email@dmacc.edu', 'Des Moines, IA', 'Graphic Design', 'nandadesign.net', 'I don''t have any set plans for myself. I''m just going with the flow to see where the world takes me.', 'Unique, Strong, Artistic', 'imgs/studentImages/default_image_165.jpg', 'imgs/studentImages/default_image_250.jpg'),
(44, 'Greg', 'Blair', 'gablair@dmacc.edu', 'Urbandle, IA', 'Graphic Design', 'dmacc.edu', 'Not to be living in the box my computer came in.', 'Helpful, Quiet, Tired', 'imgs/studentImages/Gregory Blair_165.jpg', 'imgs/studentImages/Gregory Blair_250.jpg'),
(45, 'Seth', 'Fahlenkamp', 'sethfahlenkamp@gmail.com', 'Montezuma, IA', 'Graphic Design', 'sethfahlenkamp.com', 'I want to explore and venture through various areas of design, using all my Graphic Design, Photography and writing skills. Currently, I work at Gannett, GIADC, as a digital artist. I also run CrÃ¼sh LLC, a local apparel company.', 'Honest, Patient, Cooperative', 'imgs/studentImages/Seth Fahlenkamp_165.jpg', 'imgs/studentImages/Seth Fahlenkamp_250.jpg'),
(46, 'Marc', 'Brault', 'marc.brault555@gmail.com', 'Urbandale, IA', 'Graphic Design', 'marcbrault.com', 'To have a career in Graphic Design focusing on various media design techniques. Originally, I started DMACC in the Veterinary Tech field, but after taking my first Illustration class, I realized where I needed to be.', 'Motivated, Focused, Driven', 'imgs/studentImages/Marc Brault_165.jpg', 'imgs/studentImages/Marc Brault_250.jpg'),
(47, 'Caleb ', 'Brendlinger', 'cbrendlinger98@gmail.com', 'Altoona, IA', 'Graphic Design', 'dmccc.edu', 'My short-term goal is to attain a Disney internship with Disney Packs. My life goal is to be an Imagineer and be a part of creating attractions/experiences in Disney Parks. ', 'Level-headed, Genuine, Ambitious', 'imgs/studentImages/Caleb Brendlinger_165.jpg', 'imgs/studentImages/Caleb Brendlinger_250.jpg'),
(48, 'Janson', 'Briggs', 'jansonbriggs@gmail.com', 'Des Moines, IA', 'Graphic Design', 'dominiondesignstudio.com', 'Work for Marvel Comics/DC Comics as a designer and eventually transition into an Illustrator/Animator/Concept Artist while working on my own graphic novel and script for an animated feature.', 'Determined, Non-Conformist, eccentric', 'imgs/studentImages/Janson Briggs_165.jpg', 'imgs/studentImages/Janson Briggs_250.jpg'),
(49, 'Taylor', 'Eckstrom', 'eckstrommiss@hotmail.com', 'Carlisle, IA', 'Graphic Design', 'tayloreckstrom.com', 'My career goals include becoming a creative director, lead animator, and character designer. Besides loving to draw, read and cook. I have always wanted a career in animation. I decided to do design first and it is what motivates me to be the best. ', 'Weird, Friendly, Artistic', 'imgs/studentImages/Taylor Eckstrom_165.jpg', 'imgs/studentImages/Taylor Eckstrom_250.jpg'),
(50, 'Semiu', 'Hodza', 'semiu.hodza@gmail.com', 'West Des Moines, IA', 'Graphic Design', 'semiuhodza.com', 'To create a thriving business in Web Design, Graphic Design, Photography and Video.', 'Creative, Dependable, Scrupulous', 'imgs/studentImages/Semiu Hodza_165.jpg', 'imgs/studentImages/Semiu Hodza_250.jpg'),
(51, 'Brandon', 'Johnson', 'hawkfan13@gmail.com', 'Des Moines, IA', 'Graphic Design', 'btjdesigns.com', 'My passion lies with the professional sports design industry. My dream is to be associated with either an NFL or MLB club, while I know that will possibly take some time, in the meantime a career with Nike, under armor or even Netflix would be ideal.', 'Hard-working', 'imgs/studentImages/default_image_165.jpg', 'imgs/studentImages/default_image_250.jpg'),
(52, 'Missy', 'Jones', 'majones.graphics@gmail.com', 'Williamsburg, IA', 'Graphic Design', 'majones13.myportfolio.com', 'I want to work at a local company that allows me to be able to meet and talk with clients so that I can better understand the needs and do my best to reach out to those people and give them what they need.', 'Outgoing, Creative, Determined', 'imgs/studentImages/Missy Jones_165.jpg', 'imgs/studentImages/Missy Jones_250.jpg'),
(53, 'Caleb', 'Harris', 'email@edu.com', 'Des Moines, IA', 'Video Production', 'JLCinematic.com', 'Write and direct shorts/ feature length films, produce high quality commercial projects, and advance career as an actor.', 'Dedicated, Creative, Passionate', 'imgs/studentImages/default_image_165.jpg', 'imgs/studentImages/default_image_250.jpg'),
(54, 'Kaydee', 'Jones', 'email@edu.com', 'Nevada/ Ames, IA', 'Graphic Design', 'kaydee-graphics.com', 'I want to be a Graphic Designer in a smaller company, hopefully staying home in Iowa or not too far away.', 'Friendly, Composed, Punny', 'imgs/studentImages/Kaydee Jones_165.jpg', 'imgs/studentImages/Kaydee Jones_250.jpg'),
(55, 'Aaron', 'Bandy', 'email@edu.com', 'Indianola, IA', 'Video Production', 'http://www.facebook.com/somethingcoolenterprises', 'Have a job in the film business, help anyone I can to make a great video and enjoy any opportunities that come in the future.', 'Exciting, Reliable, Trustworthy', 'imgs/studentImages/default_image_165.jpg', 'imgs/studentImages/default_image_250.jpg'),
(57, 'Mark', 'Kimball', 'mkimballo@yahoo.com', 'Prairie City', 'Graphic Design', 'www.mrkimball.com/Photography', 'Future plans are to get a entry level design job, then start a home business creating small websites.', 'Shy, Friendly, Awesome', 'imgs/studentImages/default_image_165.jpg', 'imgs/studentImages/default_image_250.jpg'),
(58, 'Ian', 'Frank', 'ian.r.frank@gmail.com', 'Clive, IA', 'Web Development', 'www.frank-dsm.com', 'Video Production and Web Development', 'Keen Learner, Rapid Prototyper, Motivated', 'imgs/studentImages/IanFinished.png', 'imgs/studentImages/IanFinished.png'),
(59, 'David ', 'Prins ', 'drprins@dmacc.edu', 'Norwalk', 'Select Program of Study', 'davidrprins.com', 'To be recognized as the guy who is responsible for crop circles', 'Tenacious, Jackhammer, Frivolous.', 'imgs/studentImages/David Prins_165.jpg', 'imgs/studentImages/David Prins_250.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `student_db`
--
ALTER TABLE `student_db`
  ADD PRIMARY KEY (`student_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `student_db`
--
ALTER TABLE `student_db`
  MODIFY `student_ID` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=60;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
